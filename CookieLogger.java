import java.io.*;
import java.util.*;
import java.util.logging.*;
import java.util.logging.Formatter;

public class CookieLogger extends Logger {
	private static CookieLogger logger = null;
	protected CookieLogger(String name) {
		super (name, null);
	}

	public static CookieLogger getLogger(String name) {
		try {
			if (logger == null) {
				LogManager.getLogManager().reset();
				logger = new CookieLogger(name);
				Handler HANDLER = new FileHandler("CookieClient.log", true);
				Formatter format = new Formatter() {
					@Override
					public String format(LogRecord arg0) {
						StringBuilder b = new StringBuilder();
						b.append(new Date());
		                b.append(" ");
		                b.append(arg0.getLevel());
		                b.append(" ");
		                b.append(arg0.getMessage());
		                b.append(System.getProperty("line.separator"));
		                return b.toString();
					}
				};
				HANDLER.setFormatter(format);
				logger.addHandler(HANDLER);
			}
			return logger;
		} catch (IOException ex) {
			System.out.println("Failed to initialize logger...");
			return null;
		}
	}
}