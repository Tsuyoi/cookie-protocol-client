import java.io.*;		// ByteArrayOutputStream, InputStream, OutputStream
import java.util.*;		// Arrays

/*
 *  CookieFramer Class
 *    - Class to facilitate the sending and receiving Cookie Protocol Framed Messages
 */
public class CookieFramer {
	// Our Delimeter
	private static int CONNEND = 0x0A;
	private static int DELIMCNT = 2;
	// Constructor
	public CookieFramer() {}
	/*
	 *	getMessage Method
	 *	  in
	 *		inStream - The InputStream from the TCP Socket
	 *	  out
	 *		byte[] - The next message on the stream, denoted by two delimeters
	 *    exceptions
	 *		IOException - Thrown on inStream.read() method
	 *		Exception - Thrown on no further bytes received before the message delimeter
	 */
	public static byte[] getMessage(InputStream inStream) throws IOException, Exception {
		// A buffer to hold bytes to create an array later
		byte[] buffer = new byte[100];
		int cnt = 0;
		// Tester for the amount of bytes read
		int bytesReadCnt;
		// A flag to signal the site of a delimeter in the previous run
		int delimeters = 0;
		// Infinite loop
		while (true) {
			try {
				// Read a single byte off the InputStream
				bytesReadCnt = inStream.read(buffer, cnt, 1);
			} catch (IOException ex) { throw ex; }
			// If we failed to read a byte, report a premature ending to the message
			if (bytesReadCnt == -1) {
				throw new Exception("Message Ended Prematurely!");
			}
			// If we encounter a delimeter
			if (buffer[cnt] == CONNEND) {
				// Signal the sighting of a delimeter
				delimeters++;
				// If we reached our delimeter count
				if (delimeters == DELIMCNT) {
					byte[] temp = new byte[cnt-DELIMCNT+1];
					for (int index = 0; index < temp.length; index++)
						temp[index] = buffer[index];
					// Return the buffer
					return temp;
				}
			} else {
				// Reset our delimeter flag
				delimeters = 0;
			}
			cnt++;
			if (buffer.length == cnt) {
				byte[] newBuffer = new byte[buffer.length*2];
				for (int index = 0; index < buffer.length; index++) {
					newBuffer[index] = buffer[index];
				}
				buffer = newBuffer;
			}
		}
	}
	/*
	 *	sendMessage Method
	 *	  in
	 *		outMsg - The ByteArray to be sent on outStream
	 *		outStream - The stream onto which we put the outMsg
	 *	  out
	 *		none
	 *	  exceptions
	 *		IOException - Thrown on outStream.write() method
	 */
	public static void sendMessage(byte[] outMsg, OutputStream outStream) throws IOException {
		try {
			// Write the formatted message to the stream
			for (int index = 0; index < outMsg.length; index++) {
				outStream.write(outMsg[index]);
			}
			// Add our delimeter to the end
			for (int index = 0; index < DELIMCNT; index++) {
				outStream.write(CONNEND);
			}
		} catch (IOException ex) { throw ex; }
	}
}