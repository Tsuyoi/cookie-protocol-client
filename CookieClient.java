import java.io.*;		// ByteArrayOutputStream, InputStream, OutputStream
import java.net.*;		// Socket, SocketServer
import java.util.*;
import java.util.logging.*;
import java.util.logging.Formatter;

public class CookieClient {
	private static CookieFramer FRAMER;
	private static CookieLogger LOGGER = CookieLogger.getLogger("global");
	// Program Entry Point
	public static void main(String argv[]) throws Exception {
		Socket foreignServer;

		ServerSocket domesticServer;
		Socket foreignServerClient;

		CookieMessage cookieA = new CookieMessage();
		CookieMessage cookieB = new CookieMessage();
		CookieMessage cookieC;
		CookieMessage cookieD;

		String server = "ozark.netlab.uky.edu";
		int port = 47105;
		int type = 0;
		int hostPort = 9876;

		if (argv.length == 1) {
			type = Integer.parseInt(argv[0]);
		}
		if (argv.length == 3) {
			server = argv[0];
			port = Integer.parseInt(argv[1]);
			type = Integer.parseInt(argv[2]);
		}

		LOGGER.info("Initiating Cookie Protocol");
		LOGGER.info("Server: " + server + ":" + port);
		LOGGER.info("Type: " + type);
		LOGGER.info("Connecting to remote server...");
		foreignServer = new Socket(server, port);
		System.out.println("[Connected to Remote Server]");
		if (!cookieA.parseResponse(FRAMER.getMessage(foreignServer.getInputStream()))) {
			LOGGER.severe("Error parsing greeting cookie!");
			LOGGER.severe(cookieA.getError());
			System.exit(1);
		}
		System.out.println("[Received Cookie - A]");
		LOGGER.info("Received Cookie(A):");
		LOGGER.info(" - Timestamp: " + cookieA.getTimeStamp());
		LOGGER.info(" - Cookie: " + cookieA.getCookie());
		switch (type) {
			case 0:
				System.out.println("[Initiating Type 0 Request]");
				LOGGER.info("Sending Type 0 Request from Cookie(A)...");
				FRAMER.sendMessage(cookieA.generateRequest(0), foreignServer.getOutputStream());
				if (!cookieB.parseResponse(FRAMER.getMessage(foreignServer.getInputStream()))) {
					LOGGER.severe("Error parsing request cookie!");
					LOGGER.severe(cookieB.getError());
					System.exit(1);
				}
				System.out.println("[Received Cookie - B]");
				LOGGER.info("Received Cookie(B):");
				LOGGER.info(" - Cookie: " + cookieB.getCookie());
				break;
			case 1:
				System.out.println("[Initiating Type 1 Request]");
				LOGGER.info("Initializing Type 1 Server...");
				domesticServer = new ServerSocket(hostPort);
				domesticServer.setSoTimeout(5000);
				LOGGER.info("Sending Type 1 Request from Cookie(A)...");
				FRAMER.sendMessage(cookieA.generateRequest(1), foreignServer.getOutputStream());
				try {
					LOGGER.info("Awaiting Server Connection to Local Server...");
					foreignServerClient = domesticServer.accept();
					LOGGER.info ("Server Connection Accepted!");
					if (!cookieB.parseResponse(FRAMER.getMessage(foreignServer.getInputStream()))) {
						LOGGER.severe("Error parsing request cookie!");
						LOGGER.severe(cookieB.getError());
						System.exit(1);
					}
					System.out.println("[Received Cookie - B]");
					LOGGER.info("Received Cookie(B):");
					LOGGER.info(" - Cookie: " + cookieB.getCookie());
					cookieC = new CookieMessage();
					LOGGER.info("Forwarding Cookie(B) from Local Server to Server...");
					FRAMER.sendMessage(cookieB.getCookieBytes(), foreignServerClient.getOutputStream());
					if (!cookieC.parseResponse(FRAMER.getMessage(foreignServerClient.getInputStream()))) {
						LOGGER.severe("Error parsing response cookie!");
						LOGGER.severe(cookieC.getError());
						System.exit(1);
					}
					System.out.println("[Received Cookie - C]");
					LOGGER.info("Received Cookie(C):");
					LOGGER.info(" - Cookie: " + cookieC.getCookie());
					cookieD = new CookieMessage();
					LOGGER.info("Sending Headless Type 0 Request from Cookie(C)...");
					FRAMER.sendMessage(cookieC.generateHeadlessRequest(), foreignServer.getOutputStream());
					if (!cookieD.parseResponse(FRAMER.getMessage(foreignServer.getInputStream()))) {
						LOGGER.severe("Error parsing response's response cookie!");
						LOGGER.severe(cookieD.getError());
						System.exit(1);
					}
					System.out.println("[Received Cookie - D]");
					LOGGER.info("Received Cookie(D):");
					LOGGER.info(" - Cookie: " + cookieD.getCookie());
				} catch (SocketTimeoutException ex) {
					LOGGER.severe("No incoming connections for 5 seconds, terminating...");
					System.exit(1);
				}
			    break;
		}
		System.out.println("[Protocol Completed Successfully]");
		LOGGER.info("Protocol Completed Successfully...");
	}
}