import java.io.*;
import java.net.*;
import java.util.*;

public class CookieServer {
	public static void main(String argv[]) throws Exception {
		ServerSocket server = new ServerSocket(6789);
		Random r = new Random();
		while (true) {
			Socket connection = server.accept();
			BufferedReader connIn = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			DataOutputStream connOut = new DataOutputStream(connection.getOutputStream());
			String cookie = new String();
			for (int index = 0; index < (20+r.nextInt(100)); index++) {
				cookie = cookie + r.nextInt(10);
			}
			String type0 = new String();
			String type1 = new String();
			for (int index = 0; index < cookie.length(); index++) {
				int tmp = cookie.charAt(index);
				tmp -= 48;
				int tmpType0Int = (((tmp + 1) % 10) + 48);
				byte[] tmpType0Byte = new byte[1];
				tmpType0Byte[0] = (byte)tmpType0Int;
				String tmpType0 = new String(tmpType0Byte, "ISO-8859-1");
				type0 = type0 + tmpType0;
				int tmpType1Int = (((10 - tmp) % 10) + 48);
				byte[] tmpType1Byte = new byte[1];
				tmpType1Byte[0] = (byte)tmpType1Int;
				String tmpType1 = new String(tmpType1Byte, "ISO-8859-1");
				type1 = type1 + tmpType1;
			}
			String output = new String("CS471G-S13 	" + new Date().toString() + "  		" + cookie + '\n' + '\n');
			System.out.println("Served Client (Greeting):");
			System.out.println(" - Cookie A: " + cookie);
			connOut.writeBytes(output);
			String response = connIn.readLine();
			String[] responseSplit = response.split(" ");
			if (responseSplit[0].equals("471type0")) {
				if (responseSplit[1].equals(type0)) {
					String cookie2 = new String();
					for (int index = 0; index < (20+r.nextInt(100)); index++) {
						cookie2 = cookie2 + r.nextInt(10);
					}
					connOut.writeBytes(new String("OK 	" + cookie2 + '\n' + '\n'));
				} else {
					connOut.writeBytes(new String("Error: 		" + "INVALID COOKIE RESULT!" + '\n' + '\n'));
				}
			} else if (responseSplit[0].equals("471type1")) {
				if (responseSplit[1].equals(type1)) {
					String[] address = responseSplit[2].split("-");
					String clientAddress = address[0];
					if (clientAddress.equals("0.0.0.0"))
						clientAddress = connection.getRemoteSocketAddress().toString();
					Socket type1server = new Socket(address[0], Integer.parseInt(address[1]));
					BufferedReader type1serverIn = new BufferedReader(new InputStreamReader(type1server.getInputStream()));
					DataOutputStream type1serverOut = new DataOutputStream(type1server.getOutputStream());
					String cookie2 = new String();
					for (int index = 0; index < (20+r.nextInt(100)); index++) {
						cookie2 = cookie2 + r.nextInt(10);
					}
					connOut.writeBytes(new String("OK 	" + cookie2 + '\n' + '\n'));
					System.out.println("Served Client (Status OK):");
					System.out.println(" - Cookie B: " + cookie2);
					if (type1serverIn.readLine().equals(cookie2)) {
						String cookie3 = new String();
						for (int index = 0; index < (20+r.nextInt(100)); index++) {
							cookie3 = cookie3 + r.nextInt(10);
						}
						String type0C = new String();
						String type1C = new String();
						for (int index = 0; index < cookie3.length(); index++) {
							int tmp = cookie3.charAt(index);
							tmp -= 48;
							int tmpType0Int = (((tmp + 1) % 10) + 48);
							byte[] tmpType0Byte = new byte[1];
							tmpType0Byte[0] = (byte)tmpType0Int;
							String tmpType0 = new String(tmpType0Byte, "ISO-8859-1");
							type0C = type0C + tmpType0;
							int tmpType1Int = (((10 - tmp) % 10) + 48);
							byte[] tmpType1Byte = new byte[1];
							tmpType1Byte[0] = (byte)tmpType1Int;
							String tmpType1 = new String(tmpType1Byte, "ISO-8859-1");
							type1C = type1C + tmpType1;
						}
						type1serverOut.writeBytes(new String(cookie3 + '\n' + '\n'));
						System.out.println("Served Client Server:");
						System.out.println(" - Cookie C: " + cookie3);
						response = connIn.readLine();
						response = connIn.readLine();
						if (response.equals(type0C)) {
							String cookie4 = new String();
							for (int index = 0; index < (20+r.nextInt(100)); index++) {
								cookie4 = cookie4 + r.nextInt(10);
							}
							connOut.writeBytes("OK 		" + cookie4 + '\n' + '\n');
							System.out.println("Served Client (Finale):");
							System.out.println(" - Cookie D: " + cookie4);
						} else {
							System.out.println("It doesn't match!");
							System.out.println(type0C);
							System.out.println(response);
						}
					}
				} else {
					connOut.writeBytes(new String("Error: 		" + "INVALID COOKIE RESULT!" + '\n' + '\n'));
				}
			} else {
				connOut.writeBytes(new String("Error: 		" + "IMPROPER REQUEST FORMAT!" + '\n' + '\n'));
			}
		}
	}
}