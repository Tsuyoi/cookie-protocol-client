import java.io.*;		// UnsupportedEncodingException
import java.util.*;		// Arrays

public class CookieMessage {
	// Static Variables
	private static final String ENCODING = "ISO-8859-1";							// Our Standard Encoding
	private static final String GREETING = "CS471G-S13";							// Our Greeting Header
	private static final String STATUSOK = "OK";									// Our Status_OK Header
	private static final String STATUSERROR = "ERROR";							// Our Status_ERROR Header
	private static final String[] REQUESTHEADERS = { "471type0", "471type1" };	// Our Request Headers
	private Cookie cookie;						// The bytes of the Cookie
	private byte[] error;						// The bytes of the Error
	private byte[] timestamp;					// The bytes of the Timestamp
	public CookieMessage() {}
	/*
	 *  generateHeadlessRequest Method
	 *		- Generates the Request Message without the Specified header
	 *    in
	 *      int type - The integer type of this request
	 *    out
	 *		byte[] - The byteArray of the generated request
	 */
	public byte[] generateHeadlessRequest() {
		return cookie.f();
	}
	/*
	 *  generateRequest Method
	 *		- Generates the Request Message of the Specified Type
	 *    in
	 *      int type - The integer type of this request
	 *    out
	 *		byte[] - The byteArray of the generated request
	 */
	public byte[] generateRequest(int type) {
		byte[] header_bytes = REQUESTHEADERS[type].getBytes();
		byte[] cookie_bytes = cookie.getCookieBytes();
		byte[] ret;
		switch (type) {
			case 1:
				cookie_bytes = cookie.g();
				String server = "0.0.0.0-9876";
				byte[] server_bytes = server.getBytes();
				ret = new byte[header_bytes.length+cookie_bytes.length+server_bytes.length+2];
				for (int index = 0; index < header_bytes.length; index++)
					ret[index] = header_bytes[index];
				ret[header_bytes.length] = 32;
				for (int index = 0; index < cookie_bytes.length; index++)
					ret[index+header_bytes.length+1] = cookie_bytes[index];
				ret[header_bytes.length+1+cookie_bytes.length] = 32;
				for (int index = 0; index < server_bytes.length; index++)
					ret[index+header_bytes.length+cookie_bytes.length+2] = server_bytes[index];
				return ret;
			default:
				cookie_bytes = cookie.f();
				ret = new byte[header_bytes.length+cookie_bytes.length+1];
				for (int index = 0; index < header_bytes.length; index++)
					ret[index] = header_bytes[index];
				ret[header_bytes.length] = 32;
				for (int index = 0; index < cookie_bytes.length; index++)
					ret[index+header_bytes.length+1] = cookie_bytes[index];
				return ret;
		}
	}
	/*
	 *  getCookie Method
	 *		- Returns the Cookie as a String
	 *    in
	 *      none
	 *    out
	 *		String - The String representation
	 */
	public String getCookie() {
		try {
			return new String(cookie.getCookieBytes(), ENCODING);
		} catch (UnsupportedEncodingException ex) {
			return "Improperly encoded cookie!";
		}
	}
	/*
	 *  getCookieBytes Method
	 *		- Returns the byteArray of the Cookie
	 *    in
	 *      none
	 *    out
	 *		byte[] - Cookie Bytes...
	 */
	public byte[] getCookieBytes() {
		return cookie.getCookieBytes();
	}
	/*
	 *  getError Method
	 *		- Returns the Error as a String
	 *    in
	 *      none
	 *    out
	 *		String - The String representation
	 */
	public String getError() {
		try {
			return new String(error, ENCODING);
		} catch (UnsupportedEncodingException ex) {
			return "Improperly encoded cookie!";
		}
	}
	/*
	 *  getTimeStamp Method
	 *		- Returns the Timestamp as a String
	 *    in
	 *      none
	 *    out
	 *		String - The String representation
	 */
	public String getTimeStamp() {
		try {
			return new String(timestamp, ENCODING);
		} catch (UnsupportedEncodingException ex) {
			return "Improperly encoded cookie!";
		}
	}
	/*
	 *  parseResponse Method
	 *		- Parses the message to extract a cookie
	 *    in
	 *      byte[] message - The byteArray containing the message encoded according to the Cookie protocol
	 *    out
	 *		boolean - Whether we have successfully parsed a valid Cookie message
	 */
	public boolean parseResponse(byte[] message) {
		try {
			String isGreeting = new String(Arrays.copyOfRange(message, 0, GREETING.length()), ENCODING);
			if (isGreeting.equals(GREETING)) {
				int index = GREETING.length();
				while (message[index] == 32 || message[index] == 9 || message[index] == 10)
					index++;
				int timestamp_end = message.length-1;
				while (message[timestamp_end] != 32 && message[timestamp_end] != 9 && message[timestamp_end] != 10)
					timestamp_end--;
				while (message[timestamp_end] == 32 || message[timestamp_end] == 9 || message[timestamp_end] == 10)
					timestamp_end--;
				timestamp_end++;
				timestamp = Arrays.copyOfRange(message, index, timestamp_end);
				index = timestamp_end;
				while (message[index] == 32 || message[index] == 9 || message[index] == 10)
					index++;
				cookie = new Cookie(Arrays.copyOfRange(message, index, message.length));
				return true;
			} else {
				String isOK = new String(Arrays.copyOfRange(message, 0, STATUSOK.length()), ENCODING);
				if (isOK.equals(STATUSOK)) {
					int index = STATUSOK.length();
					while (message[index] == 32 || message[index] == 9 || message[index] == 10)
						index++;
					cookie = new Cookie(Arrays.copyOfRange(message, index, message.length));
					return true;
				} else {
					String isError = new String(Arrays.copyOfRange(message, 0, STATUSERROR.length()), ENCODING);
					if (isError.equals(STATUSERROR)) {
						int index = STATUSERROR.length();
						while (message[index] == 32 || message[index] == 9 || message[index] == 10)
							index++;
						error = Arrays.copyOfRange(message, index, message.length);
						return false;
					} else {
						cookie = new Cookie(Arrays.copyOfRange(message, 0, message.length));
						return true;
					}
				}
			}
		} catch (UnsupportedEncodingException ex) {
			return false;
		}
	}
}