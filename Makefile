JFLAGS = -g
JC = javac
.SUFFIXES: .java .class

.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	Cookie.java \
	CookieFramer.java \
	CookieLogger.java \
	CookieMessage.java \
	CookieClient.java

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class
	$(RM) *.log
	$(RM) *.log*
