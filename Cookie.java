/*
 *  Cookie Class
 *    - A class to handle the parsing and operations involving the Cookie Message
 */
public class Cookie {
	private byte[] cookie;						// The bytes of the Cookie
	/*
	 *	Standard Constructor
	 */
	public Cookie(byte[] message) {
		// Initialize some variables to prevent errors
		cookie = message;
	}
	public byte[] f() {
		byte[] ret = new byte[cookie.length];
		for (int index = 0; index < cookie.length; index++)
			ret[index] = (byte)((((cookie[index] - 48) + 1) % 10) + 48);
		return ret;
	}
	public byte[] g() {
		byte[] ret = new byte[cookie.length];
		for (int index = 0; index < cookie.length; index++)
			ret[index] = (byte)(((10 - (cookie[index] - 48)) % 10) + 48);
		return ret;
	}
	/*
	 *  getCookieBytes Method
	 *		- Returns the byteArray of the Cookie
	 *    in
	 *      none
	 *    out
	 *		byte[] - Cookie Bytes...
	 */
	public byte[] getCookieBytes() {
		return cookie;
	}
}